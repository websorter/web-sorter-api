FROM maven:latest as maven-builder
WORKDIR /workspace
COPY . /workspace/
RUN mvn package

FROM openjdk:11
WORKDIR /app
COPY --from=maven-builder /workspace/target /app
EXPOSE $PORT

RUN apt update
RUN apt install -y wkhtmltopdf

CMD ["sh", "-c","java -DPORT=$PORT\
                      -DDB_URI=$DB_URI\
                      -DGOOGLE_SIGNIN_CLIENT_ID=$GOOGLE_SIGNIN_CLIENT_ID\
                      -DGOOGLE_SIGNIN_ISSUERS=$GOOGLE_SIGNIN_ISSUERS\
                      -DJWT_SECRET_KEY=$JWT_SECRET_KEY\
                      -jar *.jar"]