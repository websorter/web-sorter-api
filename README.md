# API WebSorter

Java Spring based API for the WebSorter.  
WebSorter is a project carried out as part of our studies at ESGI.  
If you want to see more about this project click [here](https://gitlab.com/websorter).

##Configuration
Configuration is done by passing environment variables to the JVM
```
PORT= port of the server
DB_HOST= host of the database server (localhost in your local machine)
DB_NAME= name of the database you want to connect
DB_USER= usernamer of the database user
DB_PASSWORD= password of the database user
```

## Compile an executable Jar
 ```shell script
mvn package
```

## Execute the executable Jar
````shell script
java -DPORT=serverPort \
     -DDB_HOST=databaseHost \
     -DDB_NAME=databaseName \
     -DDB_USER=databaseUserName \
     -DDB_PASSWORD=databaseUserPassword \
     -jar target/*.jar
````

## Tests
````shell script
mvn test
````