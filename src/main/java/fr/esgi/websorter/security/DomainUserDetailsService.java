package fr.esgi.websorter.security;

import fr.esgi.websorter.account.definitions.exceptions.AccountNotFoundException;
import fr.esgi.websorter.account.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DomainUserDetailsService implements UserDetailsService {
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DomainUserDetailsService(AccountRepository accountRepository,
                                    PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }
    @Override
    public UserDetails loadUserByUsername(String subject) throws UsernameNotFoundException {
        var account = this.accountRepository.findBySubject(subject)
                                            .orElseThrow(() -> new AccountNotFoundException("Account not Found"));

        return User.builder()
                    .username(account.getSubject())
                    .password(passwordEncoder.encode(account.getSubject()))
                    .roles("User")
                    .accountExpired(false)
                    .accountLocked(false)
                    .credentialsExpired(false)
                    .disabled(false)
                    .build();
    }
}
