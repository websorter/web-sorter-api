package fr.esgi.websorter.security;

import fr.esgi.websorter.security.definitions.ITokenProvider;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.util.StringUtils.hasText;

@Slf4j
@Component
public class TokenProvider implements ITokenProvider {
    private final long tokenValidityInMilliSeconds = Duration.ofMinutes(30).getSeconds() * 1000 ;
    private final byte[] secret;
    public static final String Token_Prefix = "Bearer ";
    private static final String AUTHORIZATION_KEY = "auth";

    public TokenProvider(@Value("${security.token.secret}") CharSequence secret) {
        this.secret = secret.toString().getBytes();
    }

    public String createToken( Authentication authentication) {
        String authorities = authentication.getAuthorities().stream()
                                                .map(GrantedAuthority::getAuthority)
                                                .collect(Collectors.joining(","));
        return Jwts.builder()
                    .setSubject(authentication.getName())
                    .claim(AUTHORIZATION_KEY, authorities)
                    .setExpiration(new Date(System.currentTimeMillis() + tokenValidityInMilliSeconds))
                    .signWith(SignatureAlgorithm.HS512, secret)
                    .compact();
    }

    public Authentication getAuthentication(String jwtToken) {
        Claims claims = parseToken(jwtToken).getBody();
        var authorities = Arrays.stream(claims.get(AUTHORIZATION_KEY).toString().split(","))
                                .map(SimpleGrantedAuthority::new)
                                .collect(Collectors.toList());

        User principal = new User(claims.getSubject(), "", authorities);
        return new UsernamePasswordAuthenticationToken(principal, jwtToken, authorities);
    }

    public boolean validateToken(String jwtToken) {
        try {
            parseToken(jwtToken);
            return true;
        } catch (JwtException | IllegalArgumentException exception) {
            log.info("Invalid Jwt Token");
            return false;
        }

    }

    private Jws<Claims> parseToken(String jwtToken) {
        return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jwtToken);
    }

    public String resolveToken(HttpServletRequest httpRequest) {
        String bearerToken = httpRequest.getHeader(AUTHORIZATION);
        if (hasText(bearerToken) && bearerToken.startsWith(Token_Prefix)) {
            return bearerToken.substring(Token_Prefix.length());
        }
        return null;
    }
}
