package fr.esgi.websorter.security.definitions;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface ITokenProvider {
    String createToken( Authentication authentication);
    String resolveToken(HttpServletRequest httpRequest);
    boolean validateToken(String jwtToken);
}
