package fr.esgi.websorter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSorterApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebSorterApplication.class, args);
    }
}
