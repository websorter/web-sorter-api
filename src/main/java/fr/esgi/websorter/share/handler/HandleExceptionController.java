package fr.esgi.websorter.share.handler;

import fr.esgi.websorter.account.definitions.exceptions.GoogleTokenException;
import fr.esgi.websorter.account.definitions.exceptions.WrongAppPasswordException;
import fr.esgi.websorter.share.definitions.ApiError;
import fr.esgi.websorter.share.definitions.exceptions.ResourceAlreadyExistException;
import fr.esgi.websorter.share.definitions.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class HandleExceptionController extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                    HttpHeaders headers,
                                                                    HttpStatus status,
                                                                    WebRequest request) {
       var servletWebRequest = (ServletWebRequest)request;
       var error = new ApiError(BAD_REQUEST.value(),
                                BAD_REQUEST.getReasonPhrase(),
                                parseDefaultMessage(ex.getMessage()),
                                servletWebRequest.getRequest().getRequestURI());
        return ResponseEntity.status(BAD_REQUEST).body(error);
    }

    private String parseDefaultMessage(String message) {
        String[] split = message.split("default message ");
        if(split.length > 0) {
            message = split[split.length - 1];
            message = message.substring(message.indexOf("[") + 1, message.indexOf("]"));
        }
        return message;
    }
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ApiError> handleResourceNotFoundException(ResourceNotFoundException exception,
                                                                    HttpServletRequest request) {
        var error = new ApiError(NOT_FOUND.value(),
                                 NOT_FOUND.getReasonPhrase(),
                                 exception.getMessage(),
                                request.getRequestURI());
        return ResponseEntity.status(NOT_FOUND).body(error);
    }

    @ExceptionHandler(ResourceAlreadyExistException.class)
    public ResponseEntity<ApiError> handleResourceAlreadyExistException(ResourceAlreadyExistException exception,
                                                                        HttpServletRequest request) {
        var error = new ApiError(CONFLICT.value(),
                                CONFLICT.getReasonPhrase(),
                                exception.getMessage(),
                                request.getRequestURI());
        return ResponseEntity.status(CONFLICT).body(error);
    }

    @ExceptionHandler(GoogleTokenException.class)
    public ResponseEntity<ApiError> handleGoogleTokenException(GoogleTokenException exception,
                                                               HttpServletRequest request) {
        var error = new ApiError(BAD_REQUEST.value(),
                BAD_REQUEST.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(BAD_REQUEST).body(error);
    }

    @ExceptionHandler(WrongAppPasswordException.class)
    public ResponseEntity<ApiError> handleWrongAppPasswordException(WrongAppPasswordException exception,
                                                               HttpServletRequest request) {
        var error = new ApiError(BAD_REQUEST.value(),
                BAD_REQUEST.getReasonPhrase(),
                exception.getMessage(),
                request.getRequestURI());
        return ResponseEntity.status(BAD_REQUEST).body(error);
    }
}
