package fr.esgi.websorter.share.definitions.enums;

public enum DocumentType {
    UnidentifiedPage("Failed to identify"),
    NotFoundPage("NotFound Page"), // valeur lorsque le runner ne trouve pas la page
    WelcomePage("Welcome Page"), // page d’accueil
    SignUpPage("SignUp Page"), // page d'inscription
    LoginPage("Login Page"), // Page de connexion
    ProfilePage("Profile Page"), // Page de profil
    ECommercePage("E-Commerce Page"), // Page e-commerce
    ArticlesPage("Articles Page"), // Page liste d’article
    StreamPage("Stream Page"), // Page de streaming
    InformationPage("Information Page"), // Page d’information
    ContactPage("Contact Page"), // Page de contact
    LegalDisclaimerPage("Legal Disclaimer Page"), //Page de mention légale
    FormPage("Form Page"), // Page de formulaire
    PaymentPage("Payment Page"); // Page de paiement


    private final String value;

    DocumentType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public static DocumentType fromValue(String value) {
        for( DocumentType documentType: values() ) {
            if( documentType.value.equals(value) ) {
                return documentType;
            }
        }
        return null;
    }
}
