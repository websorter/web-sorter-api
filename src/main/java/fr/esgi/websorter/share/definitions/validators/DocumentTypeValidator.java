package fr.esgi.websorter.share.definitions.validators;

import fr.esgi.websorter.share.definitions.constraints.DocumentTypeConstraint;
import fr.esgi.websorter.share.definitions.enums.DocumentType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DocumentTypeValidator implements ConstraintValidator<DocumentTypeConstraint, String> {
    @Override
    public void initialize(DocumentTypeConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return DocumentType.fromValue(s) != null;
    }
}
