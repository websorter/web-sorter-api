package fr.esgi.websorter.share.definitions;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class BaseModel {
    @Id
    private String id;
}
