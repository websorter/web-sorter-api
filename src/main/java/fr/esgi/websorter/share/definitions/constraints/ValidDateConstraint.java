package fr.esgi.websorter.share.definitions.constraints;

import fr.esgi.websorter.share.definitions.validators.ValidDateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidDateValidator.class)
@Target( {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDateConstraint {
    String message() default "Invalid Date Format";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
