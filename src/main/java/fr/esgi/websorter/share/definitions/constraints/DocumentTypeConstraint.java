package fr.esgi.websorter.share.definitions.constraints;

import fr.esgi.websorter.share.definitions.validators.DocumentTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DocumentTypeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD} )
@Retention( RetentionPolicy.RUNTIME)
public @interface DocumentTypeConstraint {
    String message() default "Invalid Document type";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
