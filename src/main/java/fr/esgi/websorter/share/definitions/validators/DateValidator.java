package fr.esgi.websorter.share.definitions.validators;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

public class DateValidator {
    public static boolean isValid(Date dateStr) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setLenient(false);
        Logger.getLogger("DateValidator").warning("dateStr : " + dateStr);
        try {
            df.parse(String.valueOf(dateStr));
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
