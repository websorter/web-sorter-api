package fr.esgi.websorter.share.definitions;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

public class DateDeSerializer extends StdDeserializer<Date> {

    public DateDeSerializer() {
        super(Date.class);
    }

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt)
            throws IOException {
        String value = p.readValueAs(String.class);
        try {
            var localDateTime = LocalDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME);
            var zonedDateTime = localDateTime.atOffset(ZoneOffset.UTC);
            return Date.from(zonedDateTime.toInstant());
        } catch (DateTimeParseException e) {
            Logger.getLogger("DateDeSerializer").warning("Wrong Date Format : " + value);
        }
        return null;
    }

}
