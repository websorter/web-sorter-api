package fr.esgi.websorter.share.definitions.validators;

import fr.esgi.websorter.share.definitions.constraints.ValidDateConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;
import java.util.logging.Logger;

public class ValidDateValidator implements ConstraintValidator<ValidDateConstraint, Date> {
    @Override
    public void initialize(ValidDateConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(Date value, ConstraintValidatorContext context) {
        return DateValidator.isValid(value);
    }
}
