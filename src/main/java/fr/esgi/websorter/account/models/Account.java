package fr.esgi.websorter.account.models;

import fr.esgi.websorter.share.definitions.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Data
@EqualsAndHashCode(callSuper = true)
@Document(collection = "Account")
public class Account extends BaseModel {

    @Field(name = "subject")
    @Indexed(unique = true)
    private String subject;

    public Account(String subject) {
        this.subject = subject;
    }
}
