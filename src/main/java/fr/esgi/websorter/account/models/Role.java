package fr.esgi.websorter.account.models;

import fr.esgi.websorter.account.enums.ERole;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "Role")
public class Role {

    @Field(name = "name")
    @Indexed()
    private ERole name;
}
