package fr.esgi.websorter.account.models;

import fr.esgi.websorter.share.definitions.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection = "User")
@Data
@EqualsAndHashCode(callSuper = true)
public class User extends BaseModel {

    @Field(name = "accountId")
    @Indexed(unique = true)
    private ObjectId accountId;

    @Field(name = "email")
    private String email;

    @Field(name = "firstName")
    private String firstName;

    @Field(name = "lastName")
    private String lastName;

    @Field(name = "pictureUrl")
    private String pictureUrl;

    @Field(name = "nationality")
    private String nationality;

    public User(String email, String firstName, String lastName, String pictureUrl, String nationality) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pictureUrl = pictureUrl;
        this.nationality = nationality;
    }
}
