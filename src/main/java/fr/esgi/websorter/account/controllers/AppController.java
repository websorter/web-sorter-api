package fr.esgi.websorter.account.controllers;

import fr.esgi.websorter.account.definitions.dtos.app.AppLoginDto;
import fr.esgi.websorter.account.definitions.exceptions.AccountNotFoundException;
import fr.esgi.websorter.account.definitions.exceptions.WrongAppPasswordException;
import fr.esgi.websorter.account.models.Account;
import fr.esgi.websorter.account.services.AppService;
import fr.esgi.websorter.security.definitions.ITokenProvider;
import fr.esgi.websorter.utils.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.logging.Logger;

import static fr.esgi.websorter.security.TokenProvider.Token_Prefix;
import static fr.esgi.websorter.utils.Routes.App.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping(path= Routes.App.Prefix, produces = MediaType.APPLICATION_JSON_VALUE)
public class AppController {

    private final AppService appService;
    private final AuthenticationManager authenticationManager;
    private final ITokenProvider tokenProvider;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AppController(AppService appService, AuthenticationManager authenticationManager, ITokenProvider tokenProvider, PasswordEncoder passwordEncoder) {
        this.appService = appService;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(Login)
    public ResponseEntity<?> login(@RequestBody @Valid AppLoginDto accountLoginDto) {
        if (!appService.exists(accountLoginDto)) {
           throw new AccountNotFoundException("App not registered");
        }
        var accountDto = appService.login(accountLoginDto.getPassword(), accountLoginDto);
        if (accountDto == null) {
            throw new WrongAppPasswordException("Wrong app password");
        }
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountDto.getSubject(), accountDto.getSubject());
        var authentication = authenticationManager.authenticate(authenticationToken);
        var token = tokenProvider.createToken(authentication);
        var httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTHORIZATION, Token_Prefix + token);
        return ResponseEntity.noContent().headers(httpHeaders).build();
    }

    @PostMapping(Register)
    public ResponseEntity<?> register(@RequestBody @Valid AppLoginDto appRegisterDto) {
        if (appService.exists(appRegisterDto)) {
            return ResponseEntity.badRequest().body("App already registered");
        }
        appService.save(appRegisterDto);
        return ResponseEntity.ok("App registered successfully");
    }
}
