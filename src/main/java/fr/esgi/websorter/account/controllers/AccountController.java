package fr.esgi.websorter.account.controllers;

import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.definitions.services.IAccountService;
import fr.esgi.websorter.security.definitions.ITokenProvider;
import fr.esgi.websorter.utils.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.security.Principal;

import static fr.esgi.websorter.security.TokenProvider.Token_Prefix;
import static fr.esgi.websorter.utils.Routes.Account.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping(path = Routes.Account.Prefix, produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

    private final IAccountService accountService;
    private final ITokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManager;

    @Autowired
    public AccountController(IAccountService accountService,
                             ITokenProvider tokenProvider,
                             AuthenticationManagerBuilder authenticationManager) {
        this.accountService = accountService;
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(Login)
    public ResponseEntity<?> login(@RequestBody @Valid AccountLoginDto accountLoginDto) {
        var accountDto = accountService.login(accountLoginDto.getIdToken());
        var authenticationToken = new UsernamePasswordAuthenticationToken(accountDto.getSubject(),
                accountDto.getSubject());

        var authentication = authenticationManager.getObject().authenticate(authenticationToken);

        var token = tokenProvider.createToken(authentication);
        var httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTHORIZATION, Token_Prefix + token);
        return ResponseEntity.noContent().headers(httpHeaders).build();
    }

    @GetMapping(Me)
    public ResponseEntity<UserDto> me(Principal principal) {
        var userDto = accountService.me(principal.getName());
        return ResponseEntity.ok(userDto);
    }

}
