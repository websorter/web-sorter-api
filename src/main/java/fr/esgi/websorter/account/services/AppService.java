package fr.esgi.websorter.account.services;

import fr.esgi.websorter.account.definitions.dtos.account.AccountDto;
import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.app.AppLoginDto;
import fr.esgi.websorter.account.definitions.services.IAppService;
import fr.esgi.websorter.account.models.Account;
import fr.esgi.websorter.account.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class AppService implements IAppService {
    private AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AppService(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AccountDto login(String password, AppLoginDto accountLoginDto) {
        var account = this.findByName(accountLoginDto);
        var toDecode = account.getSubject().split(" ")[1];
        if (passwordEncoder.matches(password, toDecode)) {
            return new AccountDto(account);
        }
        return null;
    }

    @Override
    public Boolean exists(AppLoginDto appLoginDto) {
        var allAccounts = accountRepository.findAll();
        for (Account account :
                allAccounts) {
            if (appLoginDto.getName().equals(account.getSubject().split(" ")[0])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Account findByName(AppLoginDto appLoginDto) {
        var allAccounts = accountRepository.findAll();
        for (Account account :
                allAccounts) {
            if (appLoginDto.getName().equals(account.getSubject().split(" ")[0])) {
                return account;
            }
        }
        return null;
    }

    @Override
    public Account save(AppLoginDto appLoginDto) {
        Account account = new Account(appLoginDto.getName() + " " +
                passwordEncoder.encode(appLoginDto.getPassword()));
        return accountRepository.save(account);
    }
}
