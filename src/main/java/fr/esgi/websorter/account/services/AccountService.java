package fr.esgi.websorter.account.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import fr.esgi.websorter.account.definitions.dtos.account.AccountDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.definitions.exceptions.AccountNotFoundException;
import fr.esgi.websorter.account.definitions.services.IAccountService;
import fr.esgi.websorter.account.definitions.services.IGoogleTokenVerifier;
import fr.esgi.websorter.account.models.Account;
import fr.esgi.websorter.account.repositories.AccountRepository;
import fr.esgi.websorter.account.repositories.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements IAccountService {

    private final AccountRepository accountRepository;
    private final IGoogleTokenVerifier googleTokenVerifier;
    private final UserRepository userRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository,
                          UserRepository userRepository,
                          IGoogleTokenVerifier googleTokenVerifier) {
        this.accountRepository = accountRepository;
        this.googleTokenVerifier = googleTokenVerifier;
        this.userRepository = userRepository;
    }

    @Override
    public AccountDto login(String idToken) {
        //Check with Google
        var payload =  googleTokenVerifier.validateIdToken(idToken);
        var subject = payload.getSubject();

        var optionalAccount = accountRepository.findBySubject(subject);
        //Create the account if not exist
        Account account = optionalAccount.orElseGet(() -> createAccount(payload));
        return new AccountDto(account);
    }

    @Override
    public UserDto me(String subject) {
        var account = accountRepository.findBySubject(subject)
                            .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        var user = userRepository.findByAccountId(new ObjectId(account.getId()))
                                    .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        return new UserDto(user);
    }

    private Account createAccount(Payload payload) {
        //Create the account
        var account = new Account(payload.getSubject());
        account = accountRepository.save(account);

        // create the User linked to the account
        var userToCreate = googleTokenVerifier.extractUser(payload);
        userToCreate.setAccountId(new ObjectId(account.getId()));
        userRepository.save(userToCreate);

        return account;
    }

}
