package fr.esgi.websorter.account.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import fr.esgi.websorter.account.definitions.services.IGoogleTokenVerifier;
import fr.esgi.websorter.account.models.User;
import fr.esgi.websorter.account.definitions.exceptions.GoogleTokenException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

@Component
public class GoogleTokenVerifier implements IGoogleTokenVerifier {

    private final GoogleIdTokenVerifier googleIdTokenVerifier;
    private final String clientId ;
    private final List<String> googleIssuers;

    public GoogleTokenVerifier(@Value("${google.signin.client.id}") String clientId,
                               @Value("${google.signin.issuers}") List<String> googleIssuers) {
        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        googleIdTokenVerifier = new GoogleIdTokenVerifier.Builder(httpTransport, jsonFactory)
                                // Specify the CLIENT_ID of the app that accesses the backend:
                                .setAudience(Collections.singletonList(clientId))
                                // Or, if multiple clients access the backend:
                                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                                .build();
        this.clientId = clientId;
        this.googleIssuers = googleIssuers;
    }

    public Payload validateIdToken(String token) {
        GoogleIdToken googleIdToken;
        try{
            googleIdToken = googleIdTokenVerifier.verify(token);
        } catch (GeneralSecurityException | IllegalArgumentException | IOException e) {
            System.err.println("Exception catcher " + e);
            throw new GoogleTokenException("Invalid ID Token Google");
        }

        if( googleIdToken == null ) {
            throw new GoogleTokenException("Invalid ID Token Google");
        }

        var payload = googleIdToken.getPayload();

        if( !googleIssuers.contains(payload.getIssuer())) {
            throw new GoogleTokenException("Invalid Issuer");
        }

        if( !payload.getAudience().equals(clientId) ) {
            throw new GoogleTokenException("Invalid Audience");
        }

        if ( !payload.getEmailVerified()) {
            throw new GoogleTokenException("Google email not verified");
        }

        return  payload;
    }
    public User extractUser(Payload payload) {
        var email = payload.getEmail();
        String familyName = (String) payload.get("family_name");
        String givenName = String.valueOf(payload.get("given_name"));
        String pictureUrl = (String) payload.get("picture");
        String locale = (String) payload.get("locale");
        return new User(email, givenName, familyName, pictureUrl, locale);
    }
}
