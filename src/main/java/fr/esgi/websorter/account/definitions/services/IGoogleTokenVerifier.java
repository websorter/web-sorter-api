package fr.esgi.websorter.account.definitions.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import fr.esgi.websorter.account.models.User;

public interface IGoogleTokenVerifier {
    Payload validateIdToken(String idToken);
    User extractUser(Payload payload);
}
