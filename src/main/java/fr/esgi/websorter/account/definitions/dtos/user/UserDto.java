package fr.esgi.websorter.account.definitions.dtos.user;

import fr.esgi.websorter.account.models.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String id;
    private String accountId;
    private String email;
    private String firstName;
    private String lastName;
    private String pictureUrl;
    private String nationality;

    public UserDto(User user) {
        this.id = user.getId();
        this.accountId = user.getAccountId().toString();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.pictureUrl = user.getPictureUrl();
        this.nationality = user.getNationality();
    }

    public UserDto(String email, String firstName, String lastName,
                   String pictureUrl, String nationality) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pictureUrl = pictureUrl;
        this.nationality = nationality;
    }


}
