package fr.esgi.websorter.account.definitions.exceptions;

import fr.esgi.websorter.share.definitions.exceptions.ResourceNotFoundException;

public class AccountNotFoundException extends ResourceNotFoundException {
    public AccountNotFoundException(String message) {
        super(message);
    }
}
