package fr.esgi.websorter.account.definitions.exceptions;

import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.web.client.ResourceAccessException;

public class WrongAppPasswordException extends ResourceAccessException {
    public WrongAppPasswordException(String message) {
        super(message);
    }
}
