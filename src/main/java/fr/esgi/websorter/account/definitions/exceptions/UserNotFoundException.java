package fr.esgi.websorter.account.definitions.exceptions;

import fr.esgi.websorter.share.definitions.exceptions.ResourceNotFoundException;

public class UserNotFoundException extends ResourceNotFoundException {
    public UserNotFoundException(String message) {
        super(message);
    }
}
