package fr.esgi.websorter.account.definitions.exceptions;

public class GoogleTokenException extends RuntimeException {
    public GoogleTokenException(String message) {
        super(message);
    }
}
