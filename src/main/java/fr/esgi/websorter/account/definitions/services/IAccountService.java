package fr.esgi.websorter.account.definitions.services;

import fr.esgi.websorter.account.definitions.dtos.account.AccountDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.definitions.exceptions.GoogleTokenException;


public interface IAccountService {
    AccountDto login(String idToken) throws GoogleTokenException;
    UserDto me(String subject);
}
