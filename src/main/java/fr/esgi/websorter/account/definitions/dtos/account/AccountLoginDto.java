package fr.esgi.websorter.account.definitions.dtos.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountLoginDto {
    @NotNull(message = "idToken ne peut pas etre null")
    @NotEmpty(message = "idToken ne peut pas etre empty")
    private String idToken;
}
