package fr.esgi.websorter.account.definitions.dtos.account;

import fr.esgi.websorter.account.models.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    @NotNull
    @NotEmpty
    private String id;

    @NotNull
    @NotEmpty
    private String subject;

    public AccountDto(Account account) {
        this.id = account.getId();
        this.subject = account.getSubject();
    }

    public String getName() {
        return this.subject.split(" ")[0];
    }

    public String getPassword() {
        if(this.subject.split(" ").length >= 2) {
            return this.subject.split(" ")[1];
        }
        return "";
    }

    public String getSubject() {
        return subject;
    }
}
