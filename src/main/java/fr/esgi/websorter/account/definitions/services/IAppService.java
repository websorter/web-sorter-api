package fr.esgi.websorter.account.definitions.services;

import fr.esgi.websorter.account.definitions.dtos.account.AccountDto;
import fr.esgi.websorter.account.definitions.dtos.app.AppLoginDto;
import fr.esgi.websorter.account.definitions.exceptions.AccountAlreadyExistException;
import fr.esgi.websorter.account.definitions.exceptions.AccountNotFoundException;
import fr.esgi.websorter.account.models.Account;

public interface IAppService {
    AccountDto login(String password, AppLoginDto accountLoginDto);
    Boolean exists(AppLoginDto appLoginDto);
    Account save(AppLoginDto appLoginDto) throws AccountAlreadyExistException;
    Account findByName(AppLoginDto appLoginDto) throws AccountNotFoundException;
}
