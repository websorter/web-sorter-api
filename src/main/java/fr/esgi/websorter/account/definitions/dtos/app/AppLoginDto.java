package fr.esgi.websorter.account.definitions.dtos.app;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppLoginDto {
    @NotNull(message = "name ne peut pas etre null")
    @NotEmpty(message = "name ne peut pas être empty")
    private String name;
    @NotNull(message = "password ne peut pas etre null")
    @NotEmpty(message = "password ne peut pas être empty")
    private String password;

    public String getName() {
        return name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
