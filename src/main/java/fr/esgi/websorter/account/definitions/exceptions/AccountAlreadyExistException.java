package fr.esgi.websorter.account.definitions.exceptions;

import fr.esgi.websorter.share.definitions.exceptions.ResourceAlreadyExistException;

public class AccountAlreadyExistException extends ResourceAlreadyExistException {
    public AccountAlreadyExistException(String message) {
        super(message);
    }
}
