package fr.esgi.websorter.account.repositories;

import fr.esgi.websorter.account.models.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends MongoRepository<Account, String> {
    Optional<Account> findBySubject(String subject);
    Account findAccountBySubject(String subject);
    Boolean existsBySubject(String subject);
    List<Account> findAll();
}
