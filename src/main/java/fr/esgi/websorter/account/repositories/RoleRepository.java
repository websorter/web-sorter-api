package fr.esgi.websorter.account.repositories;

import fr.esgi.websorter.account.enums.ERole;
import fr.esgi.websorter.account.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
