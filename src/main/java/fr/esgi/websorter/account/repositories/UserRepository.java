package fr.esgi.websorter.account.repositories;

import fr.esgi.websorter.account.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByAccountId(ObjectId accountId);
}
