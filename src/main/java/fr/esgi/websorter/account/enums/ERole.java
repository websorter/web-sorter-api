package fr.esgi.websorter.account.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
