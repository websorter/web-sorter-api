package fr.esgi.websorter.task.models;

import fr.esgi.websorter.share.definitions.enums.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultModel {
    @Field(name = "success")
    private boolean success;
    @Field(name = "type")
    private String type;
    @Field(name = "subject")
    private String subject;
    @Field(name = "error")
    private String error;
    @Field(name = "progression")
    private int progression;
}
