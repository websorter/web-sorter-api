package fr.esgi.websorter.task.models;

import fr.esgi.websorter.share.definitions.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Document(collection = "Task")
@AllArgsConstructor
@NoArgsConstructor
public class TaskModel extends BaseModel {
    @Field(name = "name")
    @Indexed(unique = true)
    private String name;
    @Field(name = "url")
    private String url;
    @Field(name = "createdDate")
    private Date createdDate;
    @Field(name = "executionDate")
    private Date executionDate;
    @Field(name = "result")
    private ResultModel result;
    @Field(name = "accountId")
    private ObjectId accountId;

    public TaskModel(String name, String url, String accountId) {
        this.name = name;
        this.url = url;
        this.accountId = new ObjectId(accountId);
        this.createdDate = Date.from(LocalDateTime.now().
                                                    atZone(ZoneId.systemDefault())
                                                    .toInstant());
        this.result = null;
        this.executionDate = null;
    }
}
