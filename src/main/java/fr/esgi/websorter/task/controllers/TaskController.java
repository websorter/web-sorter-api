package fr.esgi.websorter.task.controllers;

import fr.esgi.websorter.task.definitions.dtos.TaskDto;
import fr.esgi.websorter.task.definitions.dtos.task.TaskCreateDto;
import fr.esgi.websorter.task.definitions.services.ITaskService;
import fr.esgi.websorter.utils.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static fr.esgi.websorter.utils.Routes.Task.*;

@RestController
@RequestMapping(path = Routes.Task.Prefix, produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskController {

    private final ITaskService taskService ;

    @Autowired
    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping
    public ResponseEntity<TaskDto> createTask(@RequestBody @Valid TaskCreateDto taskCreateDto) {
        var task = taskService.create(taskCreateDto);
        return ResponseEntity.ok(task);
    }

    @GetMapping(path = Available)
    public ResponseEntity<TaskDto> availableTask() {
        var taskDto = taskService.available();
        return ResponseEntity.ok(taskDto);
    }

    @PutMapping(Routes.Task.Update)
    public ResponseEntity<TaskDto> updateTask(@PathVariable("taskId") String taskId,
                                              @RequestBody @Valid TaskDto taskDto) {
        taskDto = taskService.update(taskId, taskDto);
        return ResponseEntity.ok(taskDto);
    }

    @GetMapping(path = FetchAll)
    public ResponseEntity<ArrayList<TaskDto>> fetchAllTask() {
        var tasks = taskService.fetchAll();
        return ResponseEntity.ok(tasks);
    }

    @GetMapping(path = Me)
    public ResponseEntity<List<TaskDto>> fetchTasksMe(Principal principal) {
        var tasks = taskService.fetchMe(principal.getName());
        return ResponseEntity.ok(tasks);
    }

    @DeleteMapping(path = Delete)
    public ResponseEntity<TaskDto> deleteTask(@PathVariable("taskId") String taskId ) {
        var taskDto = taskService.delete(taskId);
        return ResponseEntity.ok(taskDto);
    }

}
