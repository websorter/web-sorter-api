package fr.esgi.websorter.task.repositories;

import fr.esgi.websorter.task.models.TaskModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends MongoRepository<TaskModel, String> {
    Optional<TaskModel> findByName(String name);
    List<TaskModel> findByExecutionDateNullOrderByCreatedDate();
    List<TaskModel> findAll();
    List<TaskModel> findByAccountId(ObjectId accountId);
}
