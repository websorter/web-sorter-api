package fr.esgi.websorter.task.definitions.exceptions;

import fr.esgi.websorter.share.definitions.exceptions.ResourceAlreadyExistException;

public class TaskAlreadyExistException extends ResourceAlreadyExistException {
    public TaskAlreadyExistException(String message) {
        super(message);
    }
}
