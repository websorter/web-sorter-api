package fr.esgi.websorter.task.definitions.dtos.task;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskCreateDto {
    @NotNull
    @NotEmpty(message = "name ne peut être vide")
    private String name;
    @NotNull
    @NotEmpty(message = "url ne peut être vide")
    private String url;
    @NotNull
    @NotEmpty(message = "accountId ne peut être vide")
    private String accountId;
}
