package fr.esgi.websorter.task.definitions.exceptions;

import fr.esgi.websorter.share.definitions.exceptions.ResourceNotFoundException;

public class TaskNotFountException extends ResourceNotFoundException {
    public TaskNotFountException(String message) {
        super(message);
    }
}
