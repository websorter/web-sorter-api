package fr.esgi.websorter.task.definitions.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import fr.esgi.websorter.share.definitions.DateDeSerializer;
import fr.esgi.websorter.task.models.TaskModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {
    private String id;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String url;

    @NotNull(message = "Invalid createdDate")
    @JsonDeserialize(using = DateDeSerializer.class)
    private Date createdDate;

    @NotNull(message = "Invalid executionDate")
    @JsonDeserialize(using = DateDeSerializer.class)
    private Date executionDate;

    @Valid
    private ResultDto result;

    @NotNull
    private String accountId;

    public TaskDto(TaskModel taskModel) {
        this.id = taskModel.getId();
        this.name = taskModel.getName();
        this.url = taskModel.getUrl();
        this.executionDate = taskModel.getExecutionDate();
        this.createdDate = taskModel.getCreatedDate();
        this.result = new ResultDto(taskModel.getResult());
        this.accountId = taskModel.getAccountId().toString();
    }
}
