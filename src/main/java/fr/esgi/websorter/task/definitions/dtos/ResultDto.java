package fr.esgi.websorter.task.definitions.dtos;

import fr.esgi.websorter.share.definitions.constraints.DocumentTypeConstraint;
import fr.esgi.websorter.task.models.ResultModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultDto {
    @NotNull
    private boolean success;

    @DocumentTypeConstraint
    private String type;
    private String subject;
    private String error;

    @NotNull
    @Range(min = 0, max = 100, message = "progression number should be 0 < ? < 100")
    private int progression;

    public ResultDto(ResultModel result) {
        if(result != null) {
            this.success = result.isSuccess();
            this.type = result.getType();
            this.subject = result.getSubject();
            this.error = result.getError();
            this.progression = result.getProgression();
        }
    }
}
