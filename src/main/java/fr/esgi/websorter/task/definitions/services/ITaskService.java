package fr.esgi.websorter.task.definitions.services;

import fr.esgi.websorter.account.definitions.exceptions.AccountNotFoundException;
import fr.esgi.websorter.task.definitions.dtos.TaskDto;
import fr.esgi.websorter.task.definitions.dtos.task.TaskCreateDto;
import fr.esgi.websorter.task.definitions.exceptions.TaskNotFountException;
import fr.esgi.websorter.task.definitions.exceptions.TaskAlreadyExistException;

import java.util.ArrayList;
import java.util.List;

public interface ITaskService {
    TaskDto create(TaskCreateDto taskCreateDto) throws TaskAlreadyExistException, AccountNotFoundException;
    TaskDto available() throws TaskNotFountException;
    TaskDto update(String taskId, TaskDto taskDto) throws TaskNotFountException;
    ArrayList<TaskDto> fetchAll();
    List<TaskDto> fetchMe(String subject);
    TaskDto delete(String taskId);
}
