package fr.esgi.websorter.task.services;

import fr.esgi.websorter.account.definitions.exceptions.AccountNotFoundException;
import fr.esgi.websorter.account.repositories.AccountRepository;
import fr.esgi.websorter.share.definitions.enums.DocumentType;
import fr.esgi.websorter.task.definitions.dtos.ResultDto;
import fr.esgi.websorter.task.definitions.dtos.TaskDto;
import fr.esgi.websorter.task.definitions.dtos.task.TaskCreateDto;
import fr.esgi.websorter.task.definitions.exceptions.TaskAlreadyExistException;
import fr.esgi.websorter.task.definitions.exceptions.TaskNotFountException;
import fr.esgi.websorter.task.definitions.services.ITaskService;
import fr.esgi.websorter.task.models.ResultModel;
import fr.esgi.websorter.task.models.TaskModel;
import fr.esgi.websorter.task.repositories.TaskRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService implements ITaskService {
    private final TaskRepository taskRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository,
                       AccountRepository accountRepository) {
        this.taskRepository = taskRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public TaskDto create(TaskCreateDto taskCreateDto) throws TaskAlreadyExistException, AccountNotFoundException {
        taskRepository.findByName(taskCreateDto.getName())
                        .ifPresent(taskModel ->  {
                            throw new TaskAlreadyExistException("Task with name '" +
                                                                taskCreateDto.getName() +
                                                                "'  Already Exist");
                        });
        accountRepository.findById(taskCreateDto.getAccountId())
                            .orElseThrow(() -> new AccountNotFoundException("Account with Id : '" +
                                                                            taskCreateDto.getAccountId() +
                                                                            "' not Found ."));
        var taskModel = new TaskModel(  taskCreateDto.getName(),
                                        taskCreateDto.getUrl(),
                                        taskCreateDto.getAccountId());
        taskModel = taskRepository.save(taskModel);
        return modelToDto(taskModel);
    }

    @Override
    public TaskDto available() throws TaskNotFountException {
        var listTaskSorted =  taskRepository.findByExecutionDateNullOrderByCreatedDate();
        if (listTaskSorted.size() == 0) {
            throw new TaskNotFountException("No task available");
        }
        return modelToDto(listTaskSorted.get(0));
    }

    @Override
    public TaskDto update(String taskId, TaskDto taskDto) throws TaskNotFountException{
        var taskModel = taskRepository.findById(taskId)
                        .orElseThrow(() -> new TaskNotFountException("task with Id : " + taskId + " not found."));

        taskModel.setExecutionDate(taskDto.getExecutionDate());
        var resultModel = resultDtoToResultModel(taskDto.getResult());

        taskModel.setResult(resultModel);
        taskModel = taskRepository.save(taskModel);

        return modelToDto(taskModel);
    }

    @Override
    public ArrayList<TaskDto> fetchAll() {
        var taskList  = this.taskRepository.findAll();
        ArrayList<TaskDto> results = new ArrayList<TaskDto>();
        for (TaskModel task:
             taskList) {
            results.add(modelToDto(task));
        }
        return results;
    }

    @Override
    public List<TaskDto> fetchMe(String subject) {
        var account = accountRepository.findBySubject(subject)
                                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
        var taskModels = taskRepository.findByAccountId(new ObjectId(account.getId()));
        return taskModels.stream()
                    .map(TaskDto::new)
                    .collect(Collectors.toList());
    }

    @Override
    public TaskDto delete(String taskId) {
        var task = taskRepository.findById(taskId)
                        .orElseThrow(() -> new TaskNotFountException("Task not found"));
        taskRepository.delete(task);
        return new TaskDto(task);
    }

    private TaskDto modelToDto(TaskModel taskModel) {
        var resultDto = resultModelToResultDto(taskModel.getResult());
        return new TaskDto( taskModel.getId(), taskModel.getName(), taskModel.getUrl(),
                taskModel.getCreatedDate(), taskModel.getExecutionDate(),
                resultDto, taskModel.getAccountId().toString());
    }
    private ResultDto resultModelToResultDto(ResultModel resultModel) {
        if( resultModel == null ){
            return null;
        }
        return new ResultDto(resultModel.isSuccess(), resultModel.getType(),
                            resultModel.getSubject(),resultModel.getError(),
                            resultModel.getProgression());
    }

    private ResultModel resultDtoToResultModel(ResultDto resultDto) {
        String documentType = null;
        var validDocumentType = DocumentType.fromValue(resultDto.getType());
        if(validDocumentType != null) {
            documentType = validDocumentType.toString();
        }
        return new ResultModel( resultDto.isSuccess(), documentType,
                                resultDto.getSubject(), resultDto.getError(),
                                resultDto.getProgression());
    }

}
