package fr.esgi.websorter.image;

import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
@RequestMapping(path = "/image", produces = MediaType.APPLICATION_JSON_VALUE)
public class ImageController {

    public ImageController() {

    }

    @GetMapping(produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> donwnloadImageFromUrl(@RequestParam String url) {
        Pdf pdf = new Pdf();
        pdf.addPageFromUrl(url);

        try {

            String fileName = UUID.randomUUID().toString();
            pdf.saveAs(fileName + ".pdf");
            File sourceFile = new File(fileName + ".pdf");
            if (sourceFile.exists()) {
                PDDocument document = PDDocument.load(sourceFile);
                List<PDPage> pageList = document.getDocumentCatalog().getAllPages();
                PDPage page = pageList.get(0);
                BufferedImage image = page.convertToImage();
                File pngFile = File.createTempFile(fileName, ".png");
                ImageIO.write(image, "png", pngFile);
                document.close();
                InputStream inputStream = new FileInputStream(pngFile);

                sourceFile.delete();
                pngFile.delete();

                return ResponseEntity.ok(IOUtils.toByteArray(inputStream));
            }
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).build();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).build();
        }
    }
}
