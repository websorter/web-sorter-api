package fr.esgi.websorter.hello.controllers;

import fr.esgi.websorter.utils.Routes.Hello;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = Hello.Prefix, produces = MediaType.APPLICATION_JSON_VALUE)
public class HelloController {

    @GetMapping
    public ResponseEntity<String> sayHello() {
        return ResponseEntity.ok("Hello !");
    }
}
