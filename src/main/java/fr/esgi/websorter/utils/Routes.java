package fr.esgi.websorter.utils;

public class Routes {

    static public class Hello {
        static public final String Prefix = "/hello";
    }

    static public class App {
        static public final String Prefix = "/app";
        static public final String Login = "/login";
        static public final String Register = "/register";
        public static String completeUri(String endpoint) {
            return Prefix + endpoint;
        }
    }

    static public class Account {
        static public final String Prefix = "/account";
        static public final String Login = "/login";
        static public final String Me = "/me";

        public static String completeUri(String endpoint) {
            return Prefix + endpoint;
        }
    }

    static public class Task {
        static public final String Prefix = "/task";
        static public final String Available = "/available";
        static public final String Update = "/{taskId}";
        static public final String FetchAll = "/all";
        static public final String Me = "/me";
        static public final String Delete = "/{taskId}";

        public static String completeUri(String endpoint) {
            return Prefix + endpoint;
        }
    }

    static public class Image {
        static public final String Prefix = "/image";
    }

    public static final String[] LIST_PUBLIC_ROUTES = {
        Hello.Prefix,
        Account.completeUri(Account.Login),
        App.completeUri(App.Login),
        App.completeUri(App.Register),
        Image.Prefix
    };

    public static final String[] SWAGGER_ROUTES = {
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

}
