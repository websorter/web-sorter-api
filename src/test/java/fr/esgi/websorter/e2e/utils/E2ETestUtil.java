package fr.esgi.websorter.e2e.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import io.restassured.RestAssured;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static fr.esgi.websorter.security.TokenProvider.Token_Prefix;
import static fr.esgi.websorter.utils.Routes.Account.Login;
import static fr.esgi.websorter.utils.Routes.Account.completeUri;

public class E2ETestUtil {
    private final ObjectMapper objectMapper;

    public E2ETestUtil() {
        this.objectMapper = new ObjectMapper();
    }

    public String connectUser(UserDto user) throws JsonProcessingException {

        var uri = completeUri(Login);
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(user));
        var body = objectMapper.writeValueAsString(accountLoginDto);
        return RestAssured.given()
                        .when()
                            .body(body)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .post(uri)
                        .then()
                            .statusCode(HttpStatus.NO_CONTENT.value())
                            .extract()
                            .header(HttpHeaders.AUTHORIZATION)
                            .substring(Token_Prefix.length());
    }
}
