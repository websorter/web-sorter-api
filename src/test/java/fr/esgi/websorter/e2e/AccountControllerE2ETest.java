package fr.esgi.websorter.e2e;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.e2e.utils.E2ETestUtil;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import static fr.esgi.websorter.security.TokenProvider.Token_Prefix;
import static fr.esgi.websorter.utils.Routes.Account.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AccountControllerE2ETest {

    @LocalServerPort
    private int port;
    private ObjectMapper objectMapper;
    private E2ETestUtil e2ETestUtil;
    private UserDto userTest;

    @BeforeEach
    void setup() {
        RestAssured.port = port;
        objectMapper = new ObjectMapper();
        e2ETestUtil = new E2ETestUtil();
        userTest = new UserDto("user@gmail.com","firstName","lastName",
                "https://www.google.com/url?sa=i&url=https%3A%2F%2Fapps.savoirguinee.com%2Fnode%2F29&psig=AOvVaw3uR8ImvOSqFHcdFANDFh0P&ust=1588437476640000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDk3uOMk-kCFQAAAAAdAAAAABAD",
                "fr");
    }

    @Test
    void should_connect_successfully() throws JsonProcessingException {

        var uri = completeUri(Login);
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(userTest));
        var body = objectMapper.writeValueAsString(accountLoginDto);

         RestAssured.given()
                    .when()
                        .body(body)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .post(uri)
                    .then()
                        .statusCode(NO_CONTENT.value());
    }

    @Test
    void should_get_the_information_when_authenticated() throws JsonProcessingException {
        var jwtToken = e2ETestUtil.connectUser(userTest);
        var uri = completeUri(Me);
        var user = RestAssured .given()
                    .when()
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header(new Header(AUTHORIZATION, Token_Prefix + jwtToken))
                        .get(uri)
                   .then()
                        .statusCode(OK.value())
                        .extract()
                        .jsonPath()
                        .getObject(".", UserDto.class);

        assertThat(user).isNotNull()
                        .extracting(UserDto::getEmail)
                        .isEqualTo(userTest.getEmail());
        assertThat(user).extracting(UserDto::getFirstName)
                        .isEqualTo(userTest.getFirstName());
    }
}
