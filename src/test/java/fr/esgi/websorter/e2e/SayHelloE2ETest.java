package fr.esgi.websorter.e2e;

import fr.esgi.websorter.utils.Routes;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SayHelloE2ETest {

    @LocalServerPort
    private int port;

    @BeforeEach
    void setup() {
        RestAssured.port = port;
    }

    @Test
    void should_say_hello() {
        var uri = Routes.Hello.Prefix;
        RestAssured.given()
                        .log().all()
                    .when()
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .get(uri)
                    .then()
                        .statusCode(HttpStatus.OK.value());
    }
}
