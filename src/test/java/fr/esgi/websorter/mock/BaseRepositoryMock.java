package fr.esgi.websorter.mock;

import fr.esgi.websorter.share.definitions.BaseModel;
import org.bson.types.ObjectId;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("test")
public class BaseRepositoryMock<T extends BaseModel> implements MongoRepository<T, String> {
    protected List<T> list;

    public BaseRepositoryMock() {
        list = new ArrayList<>();
    }

    @Override
    public <S extends T> S save(S s) {
        if( existsById(s.getId()) ) {
            list.remove(s);
        } else {
            var id = new ObjectId();
            s.setId(id.toString());
        }
        list.add(s);
        return s;
    }

    @Override
    public <S extends T> List<S> saveAll(Iterable<S> iterable) {
        List<S> ret = new ArrayList<>();
        iterable.forEach( s -> {
            s = save(s);
            ret.add(s);
        });
        return ret;
    }

    @Override
    public Optional<T> findById(String s) {
        return list.stream()
                    .filter( element -> s.equals(element.getId()))
                    .findFirst();
    }

    @Override
    public boolean existsById(String s) {
        return findById(s).isPresent();
    }

    @Override
    public List<T> findAll() {
        return list;
    }

    @Override
    public Iterable<T> findAllById(Iterable<String> iterable) {
        return list.stream()
                    .filter(element -> contains(iterable, element.getId()))
                    .collect(Collectors.toList());
    }

    @Override
    public long count() {
        return list.size();
    }

    @Override
    public void deleteById(String s) {
        list.removeIf(element -> s.equals(element.getId()));
    }

    @Override
    public void delete(T t) {
        list.remove(t);
    }

    @Override
    public void deleteAll(Iterable<? extends T> iterable) {
        List<T> listToDelete = new ArrayList<>();
        for(T eachElement: iterable) {
            listToDelete.add(eachElement);
        }
        list.removeAll(listToDelete);
    }

    @Override
    public void deleteAll() {
        list = new ArrayList<>();
    }

    @Override
    public List<T> findAll(Sort sort) {
        //FIXME don't know how to implement this
        return null;
    }

    @Override
    public Page<T> findAll(Pageable pageable) {
        //FIXME don't know how to implement this
        return null;
    }

    @Override
    public <S extends T> S insert(S s) {
        return save(s);
    }

    @Override
    public <S extends T> List<S> insert(Iterable<S> iterable) {
        return saveAll(iterable);
    }

    @Override
    public <S extends T> Optional<S> findOne(Example<S> example) {
       return list.stream()
               .filter(element -> element.equals(example.getProbe()))
               .map(element -> example.getProbe())
               .findFirst();
    }

    @Override
    public <S extends T> List<S> findAll(Example<S> example) {
        return list.stream()
                .filter(element -> element.equals(example.getProbe()))
                .map(element -> example.getProbe())
                .collect(Collectors.toList());
    }

    @Override
    public <S extends T> List<S> findAll(Example<S> example, Sort sort) {
        //FIXME Don't know how to implement this
        return null;
    }

    @Override
    public <S extends T> Page<S> findAll(Example<S> example, Pageable pageable) {
        //FIXME Don't know how to implement this
        return null;
    }

    @Override
    public <S extends T> long count(Example<S> example) {
        return findAll(example).size();
    }

    @Override
    public <S extends T> boolean exists(Example<S> example) {
        var existElement = list.stream()
                .filter(element -> element.equals(example.getProbe()))
                .findFirst();
        return existElement.isPresent();
    }

    private boolean contains(Iterable<String> iterable, String s) {
        for(String eachId : iterable) {
            if( eachId.equals(s) ) {
                return true;
            }
        }
        return false;
    }
}
