package fr.esgi.websorter.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.websorter.account.definitions.services.IGoogleTokenVerifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class ConfigTest {

    @Bean
    @Primary
    public IGoogleTokenVerifier googleTokenVerifier() {
        return new GoogleIdVerifierMock(new ObjectMapper());
    }

}
