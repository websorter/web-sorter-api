package fr.esgi.websorter.mock;

import fr.esgi.websorter.account.models.User;
import fr.esgi.websorter.account.repositories.UserRepository;
import org.bson.types.ObjectId;

import java.util.Optional;

public class UserRepositoryMock extends BaseRepositoryMock<User> implements UserRepository {
    @Override
    public Optional<User> findByAccountId(ObjectId accountId) {
        return this.list.stream()
                        .filter(element -> element.getAccountId().equals(accountId))
                        .findFirst();
    }
}
