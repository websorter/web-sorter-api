package fr.esgi.websorter.mock;

import fr.esgi.websorter.account.models.Account;
import fr.esgi.websorter.account.repositories.AccountRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Profile("test")
@Component
public class AccountRepositoryMock extends BaseRepositoryMock<Account> implements AccountRepository {

    public Account save(Account account) {
        if(account.getSubject() == null) return null;
        return super.save(account);
    }

    @Override
    public Optional<Account> findBySubject(String subject) {
        return list.stream()
                    .filter(account -> account.getSubject().equals(subject))
                    .findFirst();
    }

    @Override
    public Account findAccountBySubject(String subject) {
        return null; // TODO implement this
    }

    @Override
    public Boolean existsBySubject(String subject) {
        return this.findBySubject(subject).isPresent();
    }

    @Override
    public List<Account> findAll() {
        return list;
    }


}
