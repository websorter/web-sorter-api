package fr.esgi.websorter.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.definitions.services.IGoogleTokenVerifier;
import fr.esgi.websorter.account.models.User;

import java.util.UUID;

public class GoogleIdVerifierMock implements IGoogleTokenVerifier {
    private final ObjectMapper objectMapper;
    private User user;

    public GoogleIdVerifierMock(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public GoogleIdToken.Payload validateIdToken(String idToken)  {
        try {
            var user = objectMapper.readValue(idToken, UserDto.class);
            var payload = new GoogleIdToken.Payload();
            payload.setEmail(user.getEmail());
            payload.setSubject(user.getEmail());
            payload.setEmailVerified(true);
            this.user = new User(user.getEmail(), user.getFirstName(), user.getLastName(),
                            user.getPictureUrl(), user.getNationality());
            return payload;
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Override
    public User extractUser(GoogleIdToken.Payload payload) {
        return this.user;
    }
}
