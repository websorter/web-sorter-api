package fr.esgi.websorter.mock;

import fr.esgi.websorter.task.models.TaskModel;
import fr.esgi.websorter.task.repositories.TaskRepository;
import org.bson.types.ObjectId;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepositoryMock extends BaseRepositoryMock<TaskModel> implements TaskRepository {
    @Override
    public Optional<TaskModel> findByName(String name) {
        return list.stream()
                .filter(element -> element.getName().equals(name))
                .findFirst();
    }

    @Override
    public List<TaskModel> findByExecutionDateNullOrderByCreatedDate() {
        return  list.stream()
                .filter(element -> element.getExecutionDate() == null)
                .sorted(Comparator.comparingLong(task -> task.getExecutionDate().getTime()))
                .collect(Collectors.toList());
    }

    @Override
    public List<TaskModel> findByAccountId(ObjectId accountId) {
        return list.stream()
                .filter(element -> accountId.equals(element.getAccountId()))
                .collect(Collectors.toList());
    }

}
