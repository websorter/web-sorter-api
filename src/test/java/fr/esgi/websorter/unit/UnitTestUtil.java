package fr.esgi.websorter.unit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.websorter.account.definitions.dtos.account.AccountDto;
import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.repositories.AccountRepository;
import fr.esgi.websorter.account.repositories.UserRepository;
import fr.esgi.websorter.account.services.AccountService;
import fr.esgi.websorter.mock.GoogleIdVerifierMock;

public class UnitTestUtil {
    private final ObjectMapper objectMapper;
    private final AccountService accountService;

    public UnitTestUtil(AccountRepository accountRepository,
                        UserRepository userRepository) {
        objectMapper = new ObjectMapper();
        var googleTokenVerifier = new GoogleIdVerifierMock(objectMapper);
        accountService = new AccountService(accountRepository, userRepository, googleTokenVerifier);
    }

    public AccountDto login(UserDto userDto) throws JsonProcessingException {
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(userDto));
        return accountService.login(accountLoginDto.getIdToken());
    }

}
