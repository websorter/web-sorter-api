package fr.esgi.websorter.unit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.websorter.account.controllers.AccountController;
import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.services.AccountService;
import fr.esgi.websorter.security.definitions.ITokenProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.test.context.ActiveProfiles;

import java.security.Principal;

import static fr.esgi.websorter.security.TokenProvider.Token_Prefix;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@ActiveProfiles("test")
@SpringBootTest
public class AccountControllerTest {
    @Autowired
    private AccountService accountService;
    @Autowired
    private AuthenticationManagerBuilder authenticationManager;
    @Autowired
    private ITokenProvider tokenProvider;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private AccountController accountController;
    private UserDto userTest;

    private Principal newPrincipal(UserDto userDto) {
        return userDto::getEmail;
    }

    @BeforeEach
    void setup() {
        accountController = new AccountController(accountService, tokenProvider, authenticationManager);

        userTest = new UserDto("user@gmail.com","firstName","lastName",
                "https://www.google.com/url?sa=i&url=https%3A%2F%2Fapps.savoirguinee.com%2Fnode%2F29&psig=AOvVaw3uR8ImvOSqFHcdFANDFh0P&ust=1588437476640000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDk3uOMk-kCFQAAAAAdAAAAABAD",
                "fr");
    }

    @Test
    void should_login_the_userTest() throws JsonProcessingException {
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(userTest));
        var response = accountController.login(accountLoginDto);
        assertThat(response).isNotNull()
                            .isInstanceOf(ResponseEntity.class)
                            .extracting(ResponseEntity::getStatusCode)
                            .isEqualTo(NO_CONTENT);
        assertThat(response).extracting(ResponseEntity::getHeaders)
                            .extracting(httpHeaders -> httpHeaders.get(AUTHORIZATION))
                            .isNotNull();
       var authorizations  = response.getHeaders().get(AUTHORIZATION);
       assertThat(authorizations).isNotNull()
                                 .hasSize(1);
        String bearerToken = authorizations.get(0);
        var jwtToken = bearerToken.substring(Token_Prefix.length());
        assertThat(tokenProvider.validateToken(jwtToken)).isTrue();
    }

    @Test
    void should_retrieve_information_data_of_a_logged_user() throws JsonProcessingException {
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(userTest));
        accountController.login(accountLoginDto);
        var response = accountController.me(newPrincipal(userTest));
        assertThat(response).isNotNull()
                            .isInstanceOf(ResponseEntity.class)
                            .extracting(ResponseEntity::getStatusCode)
                            .isEqualTo(OK);
        assertThat(response).extracting(ResponseEntity::getBody)
                            .isInstanceOf(UserDto.class)
                            .extracting(UserDto::getEmail)
                            .isEqualTo(userTest.getEmail());
        UserDto me = response.getBody();
        assertThat(me).extracting(UserDto::getFirstName)
                        .isEqualTo(userTest.getFirstName());
        assertThat(me).extracting(UserDto::getLastName)
                .isEqualTo(userTest.getLastName());
        assertThat(me).extracting(UserDto::getNationality)
                .isEqualTo(userTest.getNationality());
        assertThat(me).extracting(UserDto::getPictureUrl)
                .isEqualTo(userTest.getPictureUrl());
    }


}
