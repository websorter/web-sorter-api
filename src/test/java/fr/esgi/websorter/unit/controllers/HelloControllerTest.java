package fr.esgi.websorter.unit.controllers;

import fr.esgi.websorter.hello.controllers.HelloController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

public class HelloControllerTest {
    private HelloController helloController;

    @BeforeEach
    void setup() {
        helloController = new HelloController();
    }

    @Test
    void sayHello_should_be_successful(){
        ResponseEntity<String> response = helloController.sayHello();
        assertThat(response).isInstanceOf(ResponseEntity.class)
                            .extracting(ResponseEntity::getStatusCode)
                            .isEqualTo(HttpStatus.OK);
        assertThat(response).extracting(ResponseEntity::getBody)
                            .isEqualTo("Hello !");

    }
}
