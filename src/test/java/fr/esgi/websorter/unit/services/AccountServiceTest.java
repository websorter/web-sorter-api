package fr.esgi.websorter.unit.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.websorter.account.definitions.dtos.account.AccountDto;
import fr.esgi.websorter.account.definitions.dtos.account.AccountLoginDto;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.account.definitions.services.IAccountService;
import fr.esgi.websorter.account.services.AccountService;
import fr.esgi.websorter.mock.AccountRepositoryMock;
import fr.esgi.websorter.mock.GoogleIdVerifierMock;
import fr.esgi.websorter.mock.UserRepositoryMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountServiceTest {

    private IAccountService accountService;
    private ObjectMapper objectMapper;
    private UserDto userTest;

    @BeforeEach
    void setup() {
        //Account Service
        var accountRepository = new AccountRepositoryMock();
        var userRepository = new UserRepositoryMock();
        objectMapper = new ObjectMapper();
        var googleTokenVerifier = new GoogleIdVerifierMock(objectMapper);
        accountService = new AccountService(accountRepository, userRepository, googleTokenVerifier);

        userTest = new UserDto("user@gmail.com","firstName","lastName",
                "https://www.google.com/url?sa=i&url=https%3A%2F%2Fapps.savoirguinee.com%2Fnode%2F29&psig=AOvVaw3uR8ImvOSqFHcdFANDFh0P&ust=1588437476640000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDk3uOMk-kCFQAAAAAdAAAAABAD",
                "fr");
    }

    @Test
    void should_connect_an_account() throws JsonProcessingException {
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(userTest));
        var accountDto = accountService.login(accountLoginDto.getIdToken());
        assertThat(accountDto).isNotNull()
                                .extracting(AccountDto::getSubject)
                                .isNotNull()
                                .isNotEqualTo("")
                                .isEqualTo(userTest.getEmail());

        assertThat(accountDto).extracting(AccountDto::getId)
                                .isNotNull()
                                .isNotEqualTo("");
    }

    @Test
    void should_retrieve_user() throws JsonProcessingException {
        var accountLoginDto = new AccountLoginDto(objectMapper.writeValueAsString(userTest));
        var accountDto = accountService.login(accountLoginDto.getIdToken());

        var userFound = accountService.me(accountDto.getSubject());
        assertThat(userFound).isNotNull()
                                .extracting(UserDto::getEmail)
                                .isEqualTo(userTest.getEmail());
        assertThat(userFound).extracting(UserDto::getFirstName)
                                .isEqualTo(userTest.getFirstName());
    }

}
