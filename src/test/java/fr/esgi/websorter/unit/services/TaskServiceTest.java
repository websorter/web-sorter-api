package fr.esgi.websorter.unit.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.esgi.websorter.account.definitions.dtos.user.UserDto;
import fr.esgi.websorter.mock.AccountRepositoryMock;
import fr.esgi.websorter.mock.TaskRepositoryMock;
import fr.esgi.websorter.mock.UserRepositoryMock;
import fr.esgi.websorter.task.definitions.dtos.ResultDto;
import fr.esgi.websorter.task.definitions.dtos.TaskDto;
import fr.esgi.websorter.task.definitions.dtos.task.TaskCreateDto;
import fr.esgi.websorter.task.definitions.services.ITaskService;
import fr.esgi.websorter.task.services.TaskService;
import fr.esgi.websorter.unit.UnitTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static fr.esgi.websorter.share.definitions.enums.DocumentType.WelcomePage;
import static org.assertj.core.api.Assertions.assertThat;


public class TaskServiceTest {

    private ITaskService taskService;
    private UnitTestUtil unitTestUtil;
    private UserDto userTest;

    @BeforeEach
    void setup() {
        var taskRepository = new TaskRepositoryMock();
        var accountRepository = new AccountRepositoryMock();
        var userRepository = new UserRepositoryMock();
        taskService = new TaskService(taskRepository, accountRepository);
        unitTestUtil = new UnitTestUtil( accountRepository, userRepository);

        userTest = new UserDto("user@gmail.com","firstName","lastName",
                "https://www.google.com/url?sa=i&url=https%3A%2F%2Fapps.savoirguinee.com%2Fnode%2F29&psig=AOvVaw3uR8ImvOSqFHcdFANDFh0P&ust=1588437476640000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDk3uOMk-kCFQAAAAAdAAAAABAD",
                "fr");
    }

    @Test
    void should_create_a_task() throws JsonProcessingException {
        //Create the account
        var accountDto = unitTestUtil.login(userTest);

        var taskCreateDto = new TaskCreateDto("Task 1",
                                                "https://spring.io/",
                                                    accountDto.getId());
        var taskDto = taskService.create(taskCreateDto);

        assertThat(taskDto).isNotNull()
                            .extracting(TaskDto::getId)
                            .isNotNull()
                            .isNotEqualTo("");
        assertThat(taskDto).extracting(TaskDto::getName)
                            .isEqualTo(taskCreateDto.getName());
        assertThat(taskDto).extracting(TaskDto::getUrl)
                            .isEqualTo(taskCreateDto.getUrl());
    }

    @Test
    void should_get_an_available_task() throws JsonProcessingException {
        var accountCreated = unitTestUtil.login(userTest);
        var taskCreateDto = new TaskCreateDto("Task 1",
                "https://www.youtube.com/watch?v=g2b-NbR48Jo",
                accountCreated.getId());
        var taskDto = taskService.create(taskCreateDto);
        var availableTask = taskService.available();

        assertThat(availableTask).isNotNull()
                                        .extracting(TaskDto::getId)
                                        .isNotNull()
                                        .isNotEqualTo("")
                                        .isEqualTo(taskDto.getId());
        assertThat(availableTask).extracting(TaskDto::getName)
                                 .isEqualTo(taskDto.getName());
        assertThat(availableTask).extracting(TaskDto::getUrl)
                                    .isEqualTo(taskDto.getUrl());
    }

    @Test
    void should_update_an_availableTask() throws JsonProcessingException {
        //Given
        var accountCreated = unitTestUtil.login(userTest);
        var taskCreateDto = new TaskCreateDto("Task 1",
                "https://spring.io/",
                accountCreated.getId());
        var taskDto = taskService.create(taskCreateDto);
        var availableTask = taskService.available();

        //When
        var now = new Date();
        availableTask.setExecutionDate(now);
        var resultDto = new ResultDto(true, WelcomePage.toString(), "Page d'accueil de Spring",
                                        null, 100);
        availableTask.setResult(resultDto);

        var taskUpdated = taskService.update(availableTask.getId(), availableTask);


        //Then
        assertThat(taskUpdated).isNotNull()
                                .extracting(TaskDto::getResult)
                                .isNotNull()
                                .extracting(ResultDto::getType)
                                .isEqualTo(resultDto.getType());

        assertThat(taskUpdated).extracting(TaskDto::getExecutionDate)
                                .isEqualTo(now);
        assertThat(taskUpdated).extracting(TaskDto::getResult)
                                .extracting(ResultDto::getSubject)
                                .isEqualTo(resultDto.getSubject());
        assertThat(taskUpdated).extracting(TaskDto::getResult)
                                .extracting(ResultDto::getProgression)
                                .isEqualTo(resultDto.getProgression());

    }
    
}
